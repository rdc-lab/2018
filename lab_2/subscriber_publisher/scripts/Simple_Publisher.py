#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def main():
    # Initialize node and allow multiple spawning
    rospy.init_node('Simple_PY_Publisher', anonymous=True)
    # Publish to the topic, create publishing handler
    publisher = rospy.Publisher("PY_String_Topic", String, queue_size=10)
    # Delay creating object
    rateHandler = rospy.Rate(5) # 5 Hz
    while not rospy.is_shutdown():
        # Message to publish
        msg_string = "Simple Python Data"
        publisher.publish(msg_string)
        rospy.loginfo("Published: [{0}]".format(msg_string))
        # Cause the delay for the rate
        rateHandler.sleep()
    

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        print("ROS interrupted, exit initiated")