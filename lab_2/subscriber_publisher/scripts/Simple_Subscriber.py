#!/usr/bin/env python
import rospy
from std_msgs.msg import String


def messageReceived(msg):
    print("Received: [{0}]".format(msg.data))

def main():
    # Initialize the node
    rospy.init_node('Simple_PY_Subscriber', anonymous=True)
    # Create a subscriber object and call the function desired whenever the message is received
    rospy.Subscriber('PY_String_Topic', String, queue_size=10, callback=messageReceived)
    # Let ROS be in a spin
    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        print("Error occured, Shutting down the node...")
