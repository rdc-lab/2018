#include "ros/ros.h"
#include "std_msgs/String.h"

void messageReceived(const std_msgs::String::ConstPtr& msg) {
    // Print that received data as a string
    ROS_INFO("Received [%s]", msg->data.c_str());
}

int main(int argc, char **argv){
    // Initialize ROS
    ros::init(argc, argv, "Simple_CPP_subscriber");
    // Create a node handler
    ros::NodeHandle nodeHandler;
    // Create a subscriber callback
    ros::Subscriber subscriberObject = nodeHandler.subscribe("CPP_String_Topic", 1000, messageReceived);
    // Let it go in an infnite loop to keep listening for messages
    ros::spin();
    return 0;
}