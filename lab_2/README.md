# Lab 2

## Please follow the following ROS Tutorials before your next Lab (14/09/18)

[Index of Tutorials](http://wiki.ros.org/ROS/Tutorials)  

From [Starting with ROS](http://wiki.ros.org/ROS/Tutorials/CreatingMsgAndSrv) till [Writing a Simple Service and Client (Python)](http://wiki.ros.org/ROS/Tutorials/UsingRosEd)  
**Complete Tutorials 10-15**

## Rechecking your code

A premade folder called subscriber_publisher can be found in the repo. In order to run the file perform the following operations:  

1. cd to your catkin_ws/src folder.

```bash
$ cd ~/catkin_ws/src/
$ ls
  
```

2. Clone the repository for 2018 and cd into it.

```bash
$ git clone git@gitlab.com:rdc-lab/2018.git
Cloning into '2018'...
remote: Enumerating objects: 41, done.
remote: Counting objects: 100% (41/41), done.
remote: Compressing objects: 100% (26/26), done.
remote: Total 41 (delta 8), reused 41 (delta 8)
Receiving objects: 100% (41/41), 6.97 KiB | 0 bytes/s, done.
Resolving deltas: 100% (8/8), done.
Checking connectivity... done.

$ cd 2018
$ ls
lab_0  lab_1  lab_2  README.md

$ cd lab_2
$ ls
README.md  subscriber_publisher

$ cd subscriber_publisher/
$ ls
CMakeLists.txt  package.xml  README.md  scripts  src
```

3. Take a look at the workspace. Open all folders and inspect the files.

4. Go to the main workspace directory and call *catkin_make*.

```bash
$ cd ~/catkin_ws/ && catkin_make
Base path: /home/dhruv/catkin_ws
Source space: /home/dhruv/catkin_ws/src
Build space: /home/dhruv/catkin_ws/build
Devel space: /home/dhruv/catkin_ws/devel
Install space: /home/dhruv/catkin_ws/install
####
#### Running command: "cmake /home/dhruv/catkin_ws/src -DCATKIN_DEVEL_PREFIX=/home/dhruv/catkin_ws/devel -DCMAKE_INSTALL_PREFIX=/home/dhruv/catkin_ws/install -G Unix Makefiles" in "/home/dhruv/catkin_ws/build"
####
-- Using CATKIN_DEVEL_PREFIX: /home/dhruv/catkin_ws/devel
-- Using CMAKE_PREFIX_PATH: /home/dhruv/catkin_ws/devel;/opt/ros/kinetic
-- This workspace overlays: /home/dhruv/catkin_ws/devel;/opt/ros/kinetic
-- Using PYTHON_EXECUTABLE: /usr/bin/python
-- Using Debian Python package layout
-- Using empy: /usr/bin/empy
-- Using CATKIN_ENABLE_TESTING: ON
-- Call enable_testing()
-- Using CATKIN_TEST_RESULTS_DIR: /home/dhruv/catkin_ws/build/test_results
-- Found gmock sources under '/usr/src/gmock': gmock will be built
-- Found gtest sources under '/usr/src/gmock': gtests will be built
-- Using Python nosetests: /usr/bin/nosetests-2.7
-- catkin 0.7.14
-- BUILD_SHARED_LIBS is on
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- ~~  traversing 1 packages in topological order:
-- ~~  - subscriber_publisher
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- +++ processing catkin package: 'subscriber_publisher'
-- ==> add_subdirectory(2018/lab_2/subscriber_publisher)
-- Configuring done
-- Generating done
-- Build files have been written to: /home/dhruv/catkin_ws/build
####
#### Running command: "make -j4 -l4" in "/home/dhruv/catkin_ws/build"
####
Scanning dependencies of target cpp_publisher
Scanning dependencies of target cpp_subscriber
[ 50%] Building CXX object 2018/lab_2/subscriber_publisher/CMakeFiles/cpp_subscriber.dir/src/Simple_Subscriber.cpp.o
[ 50%] Building CXX object 2018/lab_2/subscriber_publisher/CMakeFiles/cpp_publisher.dir/src/Simple_Publisher.cpp.o
[ 75%] Linking CXX executable /home/dhruv/catkin_ws/devel/lib/subscriber_publisher/cpp_publisher
[ 75%] Built target cpp_publisher
[100%] Linking CXX executable /home/dhruv/catkin_ws/devel/lib/subscriber_publisher/cpp_subscriber
[100%] Built target cpp_subscriber
```

This will create the nodes automatically, now just start the 

## Scripts folder

The [scripts](subscriber_publisher/scripts/) folder has the python executable files. You need to make all the python files in the directory executable  using the following command

```bash
$ cd ~/catkin_ws/src/2018/lab_2/subscriber_publisher/scripts/
~/catkin_ws/src/2018/lab_2/subscriber_publisher$

$ chmod +x *.py
Simple_Publisher.py  Simple_Subscriber.py
```

The Simple_Publisher.py and Simple_Subscriber.py will show in green.

## src folder

The [src](subscriber_publisher/src/) folder has the C++ files. You don't need to make any executable since the **catkin_make** command automatically creates object files in the build and devel folders of your **catkin_ws**.  

## Testing the package

Start **roscore**  

```bash
$ roscore
... logging to /home/dhruv/.ros/log/ce62956a-b750-11e8-8cc0-60571856adba/roslaunch-dhruv-Inspiron-7348-24406.log
Checking log directory for disk usage. This may take awhile.
Press Ctrl-C to interrupt
Done checking log file disk usage. Usage is <1GB.

started roslaunch server http://dhruv-Inspiron-7348:33101/
ros_comm version 1.12.14


SUMMARY
========

PARAMETERS
 * /rosdistro: kinetic
 * /rosversion: 1.12.14

NODES

auto-starting new master
process[master]: started with pid [24417]
ROS_MASTER_URI=http://dhruv-Inspiron-7348:11311/

setting /run_id to ce62956a-b750-11e8-8cc0-60571856adba
process[rosout-1]: started with pid [24431]
started core service [/rosout]
```

### Python

**Open a new terminal**  
Then run the nodes using **rosrun**

```bash
$ rosrun subscriber_publisher Simple_Publisher.py
[INFO] [1536842111.744401]: Published: [Simple Python Data]
[INFO] [1536842111.945062]: Published: [Simple Python Data]
[INFO] [1536842112.145066]: Published: [Simple Python Data]
[INFO] [1536842112.345126]: Published: [Simple Python Data]
[INFO] [1536842112.545063]: Published: [Simple Python Data]
[INFO] [1536842112.745059]: Published: [Simple Python Data]
[INFO] [1536842112.945110]: Published: [Simple Python Data]
[INFO] [1536842113.145112]: Published: [Simple Python Data]
[INFO] [1536842113.344914]: Published: [Simple Python Data]
[INFO] [1536842113.545034]: Published: [Simple Python Data]
[INFO] [1536842113.745078]: Published: [Simple Python Data]
[INFO] [1536842113.945076]: Published: [Simple Python Data]
.
.
.

```

**Open a new terminal**  
Then run the following command

```bash
$ rosrun subscriber_publisher Simple_Subscriber.py
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
Received: [Simple Python Data]
.
.
.

```

Press **ctrl+c** to exit.  
Close both the terminals.

### C Plus Plus

**Open a new terminal**  
Then run the nodes using **rosrun**, run these commands in separate terminals similar to what we did with the python codes.  

```bash
rosrun subscriber_publisher cpp_publisher
rosrun subscriber_publisher cpp_subscriber
```

## Analysis

You can check out the graph using `rqt_graph` command. It must look something like this<br>
![Image](DATA/RQT_GRAPH.png "Image")
You can even check out the following commands

```bash
$ rostopic list
/CPP_String_Topic
/PY_String_Topic
/rosout
/rosout_agg

$ rosnode list
/Simple_CPP_Publisher
/Simple_CPP_subscriber
/Simple_PY_Publisher_26021_1536842264175
/Simple_PY_Subscriber_26831_1536842365495
/rosout
```

Next, tinker with the code and see what changes you can do...