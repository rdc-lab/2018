# Lab 3


[Index of Tutorials](http://wiki.ros.org/urdf/Tutorials)  


1. creating a package

```bash
  $ cd ~/2018/src
  $ catkin_create_pkg lip_description urdf
  $ cd lip_description
```

2. create a folder to save the urdf file.

```bash
$ mkdir urdf
$ cd urdf

```

3. Add the .urdf file in the /urdf folder

4. create a folder to save the launch file.

5. Add the .launch file in the /launch folder


In order to test this package, perform the following steps:

Open terminal
```
$ roscore
```
Open a new window **Ctrl+Alt+T** or a tab **Ctrl+Shift+T**
```
$ cd ~/2018/
$ catkin_make
$ source devel/setup.bash
$ roslaunch lip_description rviz.launch
```
Once you launch RViz,

Click **Add** on the left panel and add the **Robot Model** parameter.

Next, click on the Dropdown with **map** written on it (**Displays -> Global Options -> Fixed Frame -> map**) and select world.

Once you perform the above steps, you'll see a model on the screen.

Feel free to test out the link controller by varying the joint states from the **joint_state_controller** popup gui that spawned along with RViz.

